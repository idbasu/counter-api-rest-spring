Build Instructions:
mvn clean package
The above command will create "counter-api.war" file in the target directory

Deployment Instructions
1. Copy the counter cunter-api.war file in the webapps directory of Tomcat. [I have used Tomcat 7.0.42]
2. Start the tomcat server
3. curl http://localhost:8080/counter-api/search -H"Authorization: Basic YWRtaW46YWRtaW4=" - d�{"searchText":["Duis", "Sed", "Donec", "Augue", "Pellentesque", "123"]}� -H"Content- Type: application/json" �X POST
4. curl http://localhost/;8080/counter-api/top/5 -H"Authorization: Basic YWRtaW46YWRtaW4=" 

Credentials:
username = admin
password = admin
This can be changed in the user.properties file located under <TOMCAT_HOME>/webapps/counter-api/WEB-INF/classes
The same user.properties file is located in the <project directory>/src/main/resources
After changing the user.properties please issue the build command again:
mvn clean package