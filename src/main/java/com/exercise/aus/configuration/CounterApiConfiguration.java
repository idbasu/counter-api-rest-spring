package com.exercise.aus.configuration;

import java.io.InputStream;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.exercise.aus.common.Constants;
import com.exercise.aus.reader.datastore.FileDataStoreReader;
import com.exercise.aus.security.HttpBasicAuthenticationEntryPoint;

@Configuration
@EnableWebMvc
@EnableWebSecurity
@ImportResource({"classpath:webapi.xml"})
@ComponentScan(basePackages = "com.exercise.aus")
public class CounterApiConfiguration extends WebSecurityConfigurerAdapter{
	public CounterApiConfiguration(){
		//System.out.println("Inside CounterAPI Configuration");
	}

	@Autowired
	public void configureGlobalSecurity(AuthenticationManagerBuilder auth) throws Exception {
		InputStream is = FileDataStoreReader.class.getClassLoader().getResourceAsStream("user.properties");
		Properties props = new Properties();
		props.load(is);
		
		auth.inMemoryAuthentication().withUser(props.getProperty("username")).password(props.getProperty("password")).roles("ADMIN");
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		String[] patternMatch = new String[]{"/search/**", "/top/**"};

		http.csrf().disable()
		.authorizeRequests()
		.antMatchers(patternMatch).hasRole("ADMIN")
		.and().httpBasic().realmName(Constants.REALM).authenticationEntryPoint(getBasicAuthEntryPoint())
		.and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
	}

	@Bean
	public HttpBasicAuthenticationEntryPoint getBasicAuthEntryPoint(){
		return new HttpBasicAuthenticationEntryPoint();
	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers(HttpMethod.OPTIONS, "/**");
	}
}