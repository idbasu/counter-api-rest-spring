package com.exercise.aus.configuration;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.springframework.web.WebApplicationInitializer;

public class SystemPropertyDefaultsInitializer implements WebApplicationInitializer{
	
	private static final Logger logger = Logger.getLogger(SystemPropertyDefaultsInitializer.class);

	@Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        logger.info("SystemPropertyWebApplicationInitializer onStartup called");
        try {
			WebApiContext.getInstance().init();
		} catch (Exception e) {
			throw new ServletException(e);
		}
    }


}
