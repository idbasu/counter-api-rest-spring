package com.exercise.aus.configuration;

import org.apache.log4j.Logger;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.exercise.aus.reader.datastore.FileDataStoreReader;


public class WebApiContext {
	
	private static AbstractApplicationContext applicationContext;
	private static final WebApiContext webApiContext = new WebApiContext();
	private static final Logger logger = Logger.getLogger(WebApiContext.class);
	
	private FileDataStoreReader fileDataStoreReader;
	
	private WebApiContext() {
		applicationContext = new ClassPathXmlApplicationContext("webapi.xml");
		applicationContext.registerShutdownHook();
	}

	public static WebApiContext getInstance() {
		return webApiContext;
	}
	
	public void init() throws Exception {
		this.initBeans();
	}
	
	private void initBeans() throws Exception {
		fileDataStoreReader = (FileDataStoreReader)applicationContext.getBean("FileDataStoreReader");
		fileDataStoreReader.init();
			
		logger.info("Initialized Beans");
	}

	public void destroy() {
		System.out.println("Calling applicationContext destroy");
	}

	public FileDataStoreReader getFileDataStoreReader() {
		return fileDataStoreReader;
	}

	public void setFileDataStoreReader(FileDataStoreReader fileDataStoreReader) {
		this.fileDataStoreReader = fileDataStoreReader;
	}	
}
