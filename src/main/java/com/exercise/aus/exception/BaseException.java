package com.exercise.aus.exception;


public class BaseException extends Exception{
    
    private static final long serialVersionUID = -7153958279159676378L;
    private int exceptionCode;
    private int statusCode;

    public BaseException(String msg) {
        super(msg);
    }
    
    public BaseException(String msg, Throwable t) {
        super(msg, t);
    }

    public void setExceptionCode(String code) {
        try {
            this.exceptionCode = Integer.parseInt(code);
        } catch (NumberFormatException e) {
            this.exceptionCode = 0;
        }
    }

    public void setExceptionCode(int code) {
        this.exceptionCode = code;
    }

    public int getExceptionCode() {
        return this.exceptionCode;
    }

    public String getExceptionCodeAsString() {
        return String.valueOf(this.exceptionCode);
    }

    /**
     * @return the statusCode
     */
    public int getStatusCode() {
        return this.statusCode;
    }

    /**
     * @param statusCode the statusCode to set
     */
    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * @param statusCode the statusCode to set
     */
    public void setStatusCode(String statusCode) {
        this.statusCode = Integer.parseInt(statusCode);
    }
    /**
     * @return the statusCode
     */
    public String getStatusCodeAsString() {
        return String.valueOf(this.statusCode);
    }
}
