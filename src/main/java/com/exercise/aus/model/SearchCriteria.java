package com.exercise.aus.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class SearchCriteria implements Serializable{
	
	private static final long serialVersionUID = -4408483789965163385L;
	
	private Set<String> searchText = new HashSet<String>();

	public Set<String> getSearchText() {
		return searchText;
	}

	public void setSearchText(Set<String> searchText) {
		this.searchText = searchText;
	}
	
	@Override
	public String toString(){
		return "toString: " + getSearchText();
	}
}
