package com.exercise.aus.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SearchResult {

	private List<Map<String, Integer>> countMap = new ArrayList<Map<String, Integer>>();

	/**
	 * @return the countMap
	 */
	public List<Map<String, Integer>> getCountMap() {
		return countMap;
	}

	/**
	 * @param countMap the countMap to set
	 */
	public void setCountMap(List<Map<String, Integer>> countMap) {
		this.countMap = countMap;
	}
	
	@Override
	public String toString(){
		return "SearchResult : " + String.valueOf(getCountMap());
	}
}
