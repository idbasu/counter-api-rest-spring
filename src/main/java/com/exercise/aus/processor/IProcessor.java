package com.exercise.aus.processor;

import com.exercise.aus.exception.BaseException;

public interface IProcessor {
	
	public void init() throws BaseException;
	public void validate() throws BaseException;
	public ProcessResult process() throws BaseException;
	public RequestTypeEnum getRequestType();
}
