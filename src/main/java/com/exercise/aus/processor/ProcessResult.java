package com.exercise.aus.processor;

public class ProcessResult<T> {
	
	private T responseContent;
	private int responseCode;

	public ProcessResult() {
		super();
	}

	public T getResponseContent() {
		return responseContent;
	}

	public void setResponseContent(T responseContent) {
		this.responseContent = responseContent;
	}

	public int getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}

}
