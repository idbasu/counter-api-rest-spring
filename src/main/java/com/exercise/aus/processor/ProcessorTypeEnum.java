package com.exercise.aus.processor;

public enum ProcessorTypeEnum {
	
	SEARCH_REQUEST_PROCESSOR, TOP_N_REQUEST_PROCESSOR;

}
