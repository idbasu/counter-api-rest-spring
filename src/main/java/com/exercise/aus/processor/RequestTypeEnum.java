package com.exercise.aus.processor;

public enum RequestTypeEnum {
	
	SEARCH_REQUEST, TOP_N_REQUEST;

}
