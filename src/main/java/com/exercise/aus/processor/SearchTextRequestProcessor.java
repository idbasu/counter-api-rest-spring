package com.exercise.aus.processor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpHeaders;
import org.springframework.util.SerializationUtils;

import com.exercise.aus.configuration.WebApiContext;
import com.exercise.aus.exception.BaseException;
import com.exercise.aus.model.SearchCriteria;
import com.exercise.aus.model.SearchResult;
import com.exercise.aus.reader.datastore.IDataStoreReader;

public class SearchTextRequestProcessor implements IProcessor {

	private byte[] body;
	private HttpHeaders headers;
	
	public SearchTextRequestProcessor() {
		// TODO Auto-generated constructor stub
	}

	public SearchTextRequestProcessor(byte[] body,HttpHeaders headers) {
		super();
		this.body = body;
		this.headers = headers;
	}

//	private List<String> validateIpOrCidr(INODetailRequest request){
//		List<String> requestValueList = request.getRecordList();
//		
//		if (requestValueList == null){
//			return null;
//		}
//
//		String ipAddress = "";
//		Iterator<String> iterator = requestValueList.iterator();
//		while (iterator.hasNext()){
//			try {
//				ipAddress = iterator.next();
//				IPUtil.isValidIpOrCidr(ipAddress);
//			} catch (IPUtilException e) {
//				logger.error("Invalid IP or CIDR:" + ipAddress);
//				iterator.remove();
//			}
//		}
//
//		return requestValueList;
//	}

	@Override
	public void init() throws BaseException {

	}

	@Override
	public void validate() throws BaseException {

	}

	@SuppressWarnings("unchecked")
	@Override
	public ProcessResult process() throws BaseException {
		ProcessResult<SearchResult> processResult = null;
		SearchResult searchResult = new SearchResult();
		
		SearchCriteria searchCriteria = (SearchCriteria)SerializationUtils.deserialize(this.body);	
		
		IDataStoreReader<Map<String, Integer>> dataStoreReader = WebApiContext.getInstance().getFileDataStoreReader(); 
			Map<String, Integer> wordCountMap = dataStoreReader.readFromStore();
			
			if (searchCriteria != null){
				Set<String> searchWordSet = searchCriteria.getSearchText();
				Map<String, Integer> countMap = new HashMap<String, Integer>();
				List<Map<String, Integer>> list = new ArrayList<Map<String,Integer>>();
				
				for (String word : searchWordSet){
					Integer occuranceCount = wordCountMap.get(word.trim().toLowerCase());
					
					countMap.put(word, occuranceCount == null ? 0 : occuranceCount);
				}
				
				list.add(countMap);
				searchResult.setCountMap(list);
			}			
		
			processResult = new ProcessResult<SearchResult>();
			processResult.setResponseContent(searchResult);
			processResult.setResponseCode(HttpServletResponse.SC_OK);
		
		return processResult;
	}

	@Override
	public RequestTypeEnum getRequestType() {
		return RequestTypeEnum.SEARCH_REQUEST;
	}
}
