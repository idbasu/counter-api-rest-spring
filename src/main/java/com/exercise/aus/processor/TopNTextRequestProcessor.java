package com.exercise.aus.processor;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpHeaders;

import com.exercise.aus.configuration.WebApiContext;
import com.exercise.aus.exception.BaseException;
import com.exercise.aus.reader.datastore.IDataStoreReader;

public class TopNTextRequestProcessor implements IProcessor {

	private byte[] body;
	private HttpHeaders headers;
	private int count;

	public TopNTextRequestProcessor() {

	}

	public TopNTextRequestProcessor(byte[] body,HttpHeaders headers) {
		super();
		this.body = body;
		this.headers = headers;
	}

	@Override
	public void init() throws BaseException {
		if(this.body != null) {
			count = Integer.parseInt(new String(this.body));
		}
	}

	@Override
	public void validate() throws BaseException {

	}

	@Override
	public ProcessResult<String> process() throws BaseException {
		ProcessResult<String> processResult = null;

		Map<String, Integer> wordCountMap = new HashMap<String, Integer>();
		StringBuffer sb = new StringBuffer();

		try{
			IDataStoreReader<Map<String, Integer>> dataStoreReader = WebApiContext.getInstance().getFileDataStoreReader(); 
			wordCountMap = dataStoreReader.topN(count);
			wordCountMap.forEach((x,y) -> sb.append(x).append(",").append(y).append("\n"));
		}catch (Exception e){
			e.printStackTrace();
		}

		processResult = new ProcessResult<String>();
		processResult.setResponseContent(String.valueOf(sb));
		processResult.setResponseCode(HttpServletResponse.SC_OK);

		return processResult;
	}

	@Override
	public RequestTypeEnum getRequestType() {
		return RequestTypeEnum.TOP_N_REQUEST;
	}
}
