package com.exercise.aus.processor.factory;

import org.springframework.http.HttpHeaders;

import com.exercise.aus.processor.IProcessor;
import com.exercise.aus.processor.ProcessorTypeEnum;
import com.exercise.aus.processor.SearchTextRequestProcessor;
import com.exercise.aus.processor.TopNTextRequestProcessor;

public class RequestProcessorFactory {
	
	private static final RequestProcessorFactory instance = new RequestProcessorFactory();

	private RequestProcessorFactory() {
		super();
	}

	public static RequestProcessorFactory getInstance() {
		return instance;
	}
	
	public IProcessor getProcessor(byte[] body,HttpHeaders headers, ProcessorTypeEnum processorType) {
		if(ProcessorTypeEnum.SEARCH_REQUEST_PROCESSOR.equals(processorType)) {
			return new SearchTextRequestProcessor(body,headers);
		} else if(ProcessorTypeEnum.TOP_N_REQUEST_PROCESSOR.equals(processorType)) {
			return new TopNTextRequestProcessor(body,headers);
		}else {
			return null;
		}
	}

}
