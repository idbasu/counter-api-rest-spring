package com.exercise.aus.reader.datastore;

import java.io.InputStream;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import com.exercise.aus.support.StringUtil;

public class FileDataStoreReader implements IDataStoreReader<Map<String, Integer>>{
	
	private static Logger logger = Logger.getLogger(FileDataStoreReader.class);
	
	private InputStream is;
	private Map<String, Integer> wordCountMap = new HashMap<String, Integer>();
	
	private String resourcePath;
	
	public FileDataStoreReader(){
		
	}
	
	public void init() throws Exception{
		logger.info("What is the resource path: " + getResourcePath());
		is = FileDataStoreReader.class.getClassLoader().getResourceAsStream(getResourcePath());
		readFromStore();
	}

	@Override
	public Map<String, Integer> readFromStore() {
		try {
			List<String> lines = IOUtils.readLines(is, "utf-8");
			String[] wordsArray;
			for (String line : lines){
				if (!StringUtil.isNullOrEmpty(line)){
					wordsArray = line.trim().split(" ");
					
					for (String word : wordsArray){
						word = word.toLowerCase().replaceAll("\\W", "");
						Integer occurance = wordCountMap.get(word);
						
						wordCountMap.put(word, occurance == null ? 0 : occurance + 1);
					}
				}
			}			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return wordCountMap;
	}	
	
	@Override
	public Map<String, Integer> topN(int level) {
		return sortByValue(wordCountMap, level);
	}
	
	private static Map<String, Integer> sortByValue(Map<String, Integer> map, int level) {
	    Map<String, Integer> result = map.entrySet().stream()
	    											.sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
	    											.limit(level)
	    											.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (x, y) -> {throw new AssertionError();},LinkedHashMap::new));
	    
	    return result;
	}

	public String getResourcePath() {
		return resourcePath;
	}

	public void setResourcePath(String resourcePath) {
		this.resourcePath = resourcePath;
	}
}
