package com.exercise.aus.reader.datastore;

public interface IDataStoreReader<T> {

	public T readFromStore();
	public T topN(int level);
}
