package com.exercise.aus.restws;

import org.apache.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.SerializationUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.exercise.aus.exception.BaseException;
import com.exercise.aus.model.SearchCriteria;
import com.exercise.aus.model.SearchResult;
import com.exercise.aus.processor.ProcessResult;
import com.exercise.aus.processor.ProcessorTypeEnum;
import com.exercise.aus.support.RequestHelper;

@RestController
public class ContentApiServiceContoller implements IContentApiService{
	
	private final static Logger logger = Logger.getLogger(ContentApiServiceContoller.class);
		
	@Override
	public @ResponseBody SearchResult search(@RequestBody SearchCriteria searchCriteria, @RequestHeader HttpHeaders headers) throws BaseException {
		logger.info("Search Criteria " + searchCriteria);
		
		byte[] bytes = null;
			try{
				bytes = SerializationUtils.serialize(searchCriteria);
			}catch(Exception e){
				e.printStackTrace();
			}
			@SuppressWarnings("unchecked")
			ProcessResult<SearchResult> result = (ProcessResult<SearchResult>) RequestHelper.handleRequest(bytes, headers, 
													ProcessorTypeEnum.SEARCH_REQUEST_PROCESSOR);
			SearchResult searchResult = result.getResponseContent();
		
		return searchResult;
	}

	@Override
	public ResponseEntity<String> topN(@PathVariable("count") int count, @RequestHeader HttpHeaders headers) throws BaseException {
		logger.info("Top " + count);
		
		@SuppressWarnings("unchecked")
		ProcessResult<String> result = (ProcessResult<String>) RequestHelper.handleRequest(RequestHelper.buildTopNCountGetRequestBody(String.valueOf(count)), 
										headers, ProcessorTypeEnum.TOP_N_REQUEST_PROCESSOR);
				
		return new ResponseEntity<String>(result.getResponseContent(), HttpStatus.OK);
	}
}
