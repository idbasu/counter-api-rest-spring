package com.exercise.aus.restws;

import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.exercise.aus.exception.BaseException;
import com.exercise.aus.model.SearchCriteria;
import com.exercise.aus.model.SearchResult;

public interface IContentApiService {
	
	@RequestMapping(value="/search", method=RequestMethod.POST)
	public @ResponseBody SearchResult search(@RequestBody SearchCriteria searCriteria, @RequestHeader HttpHeaders headers) throws BaseException;
			
	@RequestMapping(value="/top/{count}", method=RequestMethod.GET, produces="application/json")
	public ResponseEntity<String> topN(@PathVariable("count") final int count, @RequestHeader HttpHeaders headers) throws BaseException;
}
