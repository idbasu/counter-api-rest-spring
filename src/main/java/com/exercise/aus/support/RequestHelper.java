package com.exercise.aus.support;

import java.nio.charset.StandardCharsets;

import org.springframework.http.HttpHeaders;

import com.exercise.aus.exception.BaseException;
import com.exercise.aus.processor.IProcessor;
import com.exercise.aus.processor.ProcessResult;
import com.exercise.aus.processor.ProcessorTypeEnum;
import com.exercise.aus.processor.factory.RequestProcessorFactory;


public final class RequestHelper {

	private RequestHelper() {
		super();
	}
	
	public static ProcessResult<?> handleRequest(byte[] body,HttpHeaders headers, ProcessorTypeEnum processorType) throws BaseException {
		IProcessor processor = RequestProcessorFactory.getInstance().getProcessor(body,headers,processorType);
		processor.init();
		processor.validate();
		ProcessResult<?> result = processor.process();
		
		return result;
	}
	
	public static byte[] buildTopNCountGetRequestBody(String count) {
		return (count).getBytes(StandardCharsets.UTF_8);
	}
}
