package com.exercise.aus.support;

import java.io.FileNotFoundException;
import java.io.InputStream;

public final class StreamHelper {
	
	public static InputStream getInputStreamFromFile(String path) throws Exception{
		String START_PREFIX_1="./";
    	String START_PREFIX_2="/";
    	InputStream stream = null;
    	try{
	    	if(path != null){
	    		if(path.startsWith(START_PREFIX_1)){
	    			path = path.substring(2);
	    		}
	    		if(path.startsWith(START_PREFIX_2)){
	    			path = path.substring(1);
	    		}   		
	    		stream = StreamHelper.class.getClassLoader().getResourceAsStream(path);
	    		
	    		if (stream == null){
	    			throw new FileNotFoundException(path + " : Not found!!!!!");
	    		}
	    	}
    	}catch(Exception e){
    		throw new Exception(e.getMessage());
    	}
    	return stream;
    }
	
}
