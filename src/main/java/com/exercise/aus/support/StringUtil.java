package com.exercise.aus.support;

public final class StringUtil {

	public static boolean isNullOrEmpty(String s){
		return ((s == null) || s.trim().length() <= 0 );
	}
}
