package com.exercise.aus;

import java.util.HashSet;
import java.util.Set;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.exercise.aus.model.SearchCriteria;
import com.exercise.aus.model.SearchResult;

@Test
@EnableWebMvc
@WebAppConfiguration
@EnableWebSecurity
@ContextConfiguration(locations = {"classpath:spring-test-config.xml"})
public class TestCounterApi extends AbstractTestNGSpringContextTests{

	public static final String SERVER_URI = "http://localhost:8080/counter-api/";
	private static MultiValueMap<String, String> headers;
	
	@BeforeTest
	private static void addHeader(){
		headers = new LinkedMultiValueMap<String, String>();
		headers.add("Authorization", "Basic " + "YWRtaW46YWRtaW4=");
		headers.add("Content-Type", "application/json");
	}

	@Test
	private static void testCounterApiSearch() {
		RestTemplate restTemplate = new RestTemplate();
		
		String url = String.join("", SERVER_URI, "search/");
		SearchCriteria searchCriteria = new SearchCriteria();
		Set<String> searchTextSet = new HashSet<String>();
		searchTextSet.add("Duis");
		searchTextSet.add("Donec");
		searchTextSet.add("Augue");
		searchTextSet.add("Pellentesque");
		searchTextSet.add("123");
		
		HttpEntity<SearchCriteria> request = new HttpEntity<SearchCriteria>(searchCriteria, headers);
		
		searchCriteria.setSearchText(searchTextSet);
		
		SearchResult response = restTemplate.postForObject(url, request, SearchResult.class);
		System.out.println("Response: " + response);
	}

	@Test
	private static void testCounterApiTopN() {
		RestTemplate restTemplate = new RestTemplate();
		String url = String.join("", SERVER_URI, "top/20");
		
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", "Basic " + "YWRtaW46YWRtaW4=");
		headers.set("Content-Type", "application/json");

		HttpEntity request = new HttpEntity<>(headers);
		
		ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, request, String.class);
		System.out.println("Response :" + response);
	}
}
